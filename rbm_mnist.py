

import time
import sys
import random
import numpy as np
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import matplotlib.pyplot as plt

##############################################################################################################
start_time = time.time()
random.seed(23)
###############################################################################################################
# RBM model

VISIBLE_NODES = 784
HIDDEN_NODES = 300
LEARNING_RATE = 0.01


mnist = input_data.read_data_sets("MNIST/")

raw_data = mnist.train.images

print("raw data:", type(raw_data), raw_data.shape)

print("mnist.train.images:", type(mnist.train.images),mnist.train.images.shape)
print("mnist.test.images:", type(mnist.test.images),mnist.test.images.shape)
print("mnist.train.labels:", mnist.train.labels, type(mnist.train.labels), mnist.train.labels.shape)
print("mnist.test.labels:", mnist.test.labels, type(mnist.test.labels), mnist.test.labels.shape)

# print('!!!!', mnist.train.labels[1])
# x = mnist.train.images[1]
# x = x.reshape([28,28])

# plt.imshow(x)
# plt.show()


# sys.exit()

###############################################################################################################
# construction of the RBM computational graph


input_placeholder = tf.placeholder("float", shape=(None, VISIBLE_NODES))

#
weights = tf.Variable(tf.random_normal((VISIBLE_NODES, HIDDEN_NODES), mean=0.0, stddev=1. / VISIBLE_NODES))
hidden_bias = tf.Variable(tf.zeros([HIDDEN_NODES]))
visible_bias = tf.Variable(tf.zeros([VISIBLE_NODES]))

# ------------------------
hidden_activation = tf.nn.sigmoid(tf.matmul(input_placeholder, weights) + hidden_bias)  # 1
visible_reconstruction = tf.nn.sigmoid(tf.matmul(hidden_activation, tf.transpose(weights)) + visible_bias)  # 2

final_hidden_activation = tf.nn.sigmoid(tf.matmul(visible_reconstruction, weights) + hidden_bias)  # 3

# ------------------------
positive_phase = tf.matmul(tf.transpose(input_placeholder), hidden_activation)
negative_phase = tf.matmul(tf.transpose(visible_reconstruction), final_hidden_activation)
# ------------------------
weight_update = weights.assign_add(LEARNING_RATE * (positive_phase - negative_phase))   # 1
visible_bias_update = visible_bias.assign_add(LEARNING_RATE *
                                              tf.reduce_mean(input_placeholder - visible_reconstruction, 0))  # 2
hidden_bias_update = hidden_bias.assign_add(LEARNING_RATE *
                                            tf.reduce_mean(hidden_activation - final_hidden_activation, 0))  # 3
# ------------------------
train_op = tf.group(weight_update, visible_bias_update, hidden_bias_update)

loss_op = tf.reduce_sum(tf.square(input_placeholder - visible_reconstruction))

# end of construction of RBM computational graph
###############################################################################################################

session = tf.Session()

session.run(tf.global_variables_initializer())

current_epochs = 0

for i in range(10):
    total_loss = 0
    while mnist.train.epochs_completed == current_epochs:

        batch_inputs, batch_labels = mnist.train.next_batch(100)

        # print("@@@@@@@@@")
        # print(batch_inputs, batch_labels)

        _, reconstruction_loss = session.run([train_op, loss_op], feed_dict={input_placeholder: batch_inputs})

        total_loss += reconstruction_loss

    print("epochs %s reconstruction_loss: %s" % (current_epochs, reconstruction_loss))
    current_epochs = mnist.train.epochs_completed


reconstruction = session.run(visible_reconstruction, feed_dict={input_placeholder:[mnist.train.images[0]]})
reconstruction_reshape = reconstruction.reshape(28, 28)
#################################################################################################################

print('-------------------------------------------------------------')
print("original data type:", type(mnist))

print("reconstruction type:", type(reconstruction))
print("reconstruction shape:",reconstruction.shape)

print("reconstruction_reshape type:", type(reconstruction_reshape))
print("reconstruction_reshape shape:", reconstruction_reshape.shape)


#################################################################################################################
print('-------------------------------------------------------------')
end_time = time.time()
print("Program's Execution time: %d second." % (end_time - start_time))

# plt.imshow(reconstruction)
# plt.show()
plt.imshow(reconstruction_reshape)
plt.show()






