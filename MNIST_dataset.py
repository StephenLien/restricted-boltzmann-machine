


import numpy as np
import pandas as pd
from keras.utils import np_utils
from keras.datasets import mnist
import matplotlib.pyplot as plt

################################################################################
def plot_image(image, show_plt=False):
    fig = plt.gcf()
    fig.set_size_inches(5, 5)
    plt.imshow(image, cmap='binary')
    if show_plt:
        plt.show()

#-------------------------------------------------------------------------------
def plot_images_labels_prediction(fgtitle, images, labels, prediction, idx, num=10, show_plt=False):
    fig = plt.gcf()
    fig.set_size_inches(15, 40)
    plt.suptitle(fgtitle)

    if num > 25:
        num = 25
    for i in range(0, num):
        ax = plt.subplot(5, 5, 1+i)
        ax.imshow(images[idx], cmap='binary')
        title = "label=" + str(labels[idx])
        if len(prediction) > 0:
            title += ", predict=" + str(prediction[idx])

        ax.set_title(title, fontsize=10)
        ax.set_xticks([]); ax.set_yticks([])
        idx += 1
    if show_plt:


        plt.show()


################################################################################


# Load data
np.random.seed(10)
(x_train_image, y_train_label), (x_test_image, y_test_label) = mnist.load_data("/Users/jrhualien/PycharmProjects/RBM/MNIST/mnist.npz")

# Print dataset size
print('train data =', len(x_train_image))
print('test data =', len(x_test_image))


# Print dataset shape
print('x_train_image:', x_train_image.shape)
print('y_train_label:', y_train_label.shape)

print('x_test_image:', x_test_image.shape)
print('y_test_label:', y_test_label.shape)


# Plot image
plot_image(x_train_image[0], show_plt=True)
print('y_train_label[0] =', y_train_label[0])


# Plot multiple images with labels
plot_images_labels_prediction("train data", x_train_image, y_train_label, [], 0, 10, show_plt=True)
plot_images_labels_prediction("test data", x_test_image, y_test_label, [], 0, 10, show_plt=True)


print(type(x_train_image[0]), x_train_image.shape[0])




