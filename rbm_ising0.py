

from util import *
from settings import *
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

##############################################################################################################
start_time = time.time()
random.seed(23)
###############################################################################################################
# RBM model

VISIBLE_NODES = 5
HIDDEN_NODES = 100
LEARNING_RATE = 0.01
seed = 7
test_size = 0.33



# Getting back the objects:
with open(PROJECT_DIR + 'objs_2300.pkl', 'rb') as f:  # Python 3: open(..., 'rb')
    T, E, M, C, X = pickle.load(f)


df0 = pd.DataFrame()
df0["T"] = T
df0["E"] = E
df0["M"] = M
df0["C"] = C
df0["X"] = X


# Turn DF into 2d array & normalize
X = df0.values
min_max_scaler = preprocessing.MinMaxScaler()
X_scaled = min_max_scaler.fit_transform(X)

X_train, X_test = train_test_split(X_scaled, test_size=test_size, random_state=seed)
X_train = Dataset_ftr(X_train)



###############################################################################################################
# construction of the RBM computational graph


input_placeholder = tf.placeholder("float", shape=(None, VISIBLE_NODES))

#
weights = tf.Variable(tf.random_normal((VISIBLE_NODES, HIDDEN_NODES), mean=0.0, stddev=1. / VISIBLE_NODES))
hidden_bias = tf.Variable(tf.zeros([HIDDEN_NODES]))
visible_bias = tf.Variable(tf.zeros([VISIBLE_NODES]))

# ------------------------
hidden_activation = tf.nn.sigmoid(tf.matmul(input_placeholder, weights) + hidden_bias)  # 1
visible_reconstruction = tf.nn.sigmoid(tf.matmul(hidden_activation, tf.transpose(weights)) + visible_bias)  # 2

final_hidden_activation = tf.nn.sigmoid(tf.matmul(visible_reconstruction, weights) + hidden_bias)  # 3

# ------------------------
positive_phase = tf.matmul(tf.transpose(input_placeholder), hidden_activation)
negative_phase = tf.matmul(tf.transpose(visible_reconstruction), final_hidden_activation)
# ------------------------
weight_update = weights.assign_add(LEARNING_RATE * (positive_phase - negative_phase))   # 1
visible_bias_update = visible_bias.assign_add(LEARNING_RATE *
                                              tf.reduce_mean(input_placeholder - visible_reconstruction, 0))  # 2
hidden_bias_update = hidden_bias.assign_add(LEARNING_RATE *
                                            tf.reduce_mean(hidden_activation - final_hidden_activation, 0))  # 3
# ------------------------
train_op = tf.group(weight_update, visible_bias_update, hidden_bias_update)

loss_op = tf.reduce_sum(tf.square(input_placeholder - visible_reconstruction))

# end of construction of RBM computational graph
########################################################################################################################

session = tf.Session()

session.run(tf.global_variables_initializer())

current_epochs = 0

for i in range(10):
    total_loss = 0
    a = X_train.epochs_completed
    while X_train.epochs_completed == current_epochs:

        batch_inputs= X_train.next_batch(20)

        _, reconstruction_loss = session.run([train_op, loss_op], feed_dict={input_placeholder: batch_inputs})

        total_loss += reconstruction_loss

    print("epochs %s reconstruction_loss: %s" % (current_epochs, reconstruction_loss))
    current_epochs = X_train._epochs_completed


# reconstruction = session.run(visible_reconstruction, feed_dict={input_placeholder:[mnist.train.images[0]]})
# reconstruction_reshape = reconstruction.reshape(28, 28)
#################################################################################################################

# print('-------------------------------------------------------------')
# print("original data type:", type(mnist))
#
# print("reconstruction type:", type(reconstruction))
# print("reconstruction shape:",reconstruction.shape)
#
# print("reconstruction_reshape type:", type(reconstruction_reshape))
# print("reconstruction_reshape shape:", reconstruction_reshape.shape)


#################################################################################################################
print('-------------------------------------------------------------')
end_time = time.time()
print("Program's Execution time: %d second." % (end_time - start_time))

# plt.imshow(reconstruction)
# plt.show()
# plt.imshow(reconstruction_reshape)
# plt.show()






