from settings import *
from util import *
import random
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import matplotlib.pyplot as plt


def figure_noise(config, prob=0.5, alpha=0):
    '''Monte Carlo move using Metropolis algorithm '''
    rows = np.size(config, 0)
    cols = np.size(config, 1)
    for i in range(rows):
        for j in range(cols):
                a = np.random.randint(0, rows)
                b = np.random.randint(0, cols)
                s =  config[a, b]
                if s == 0:
                    pass
                elif np.random.rand() < prob:
                    s *= alpha
                config[a, b] = s
    return config

random.seed(23)

VISIBLE_NODES = 784
HIDDEN_NODES = 300
LEARNING_RATE = 0.01

mnist = input_data.read_data_sets("MNIST/")



##############################################################


input_placeholder = tf.placeholder("float", shape=(None, VISIBLE_NODES))


# print(type(input_placeholder))

weights = tf.Variable(tf.random_normal((VISIBLE_NODES, HIDDEN_NODES), mean=0.0, stddev=1. / VISIBLE_NODES))
hidden_bias = tf.Variable(tf.zeros([HIDDEN_NODES]))
visible_bias = tf.Variable(tf.zeros([VISIBLE_NODES]))

hidden_activation = tf.nn.sigmoid(tf.matmul(input_placeholder, weights) + hidden_bias)
visible_reconstruction = tf.nn.sigmoid(tf.matmul(hidden_activation, tf.transpose(weights)) + visible_bias)

final_hidden_activation = tf.nn.sigmoid(tf.matmul(visible_reconstruction, weights) + hidden_bias)

positive_phase = tf.matmul(tf.transpose(input_placeholder), hidden_activation)
negative_phase = tf.matmul(tf.transpose(visible_reconstruction), final_hidden_activation)

weight_update = weights.assign_add(LEARNING_RATE * (positive_phase - negative_phase))
visible_bias_update = visible_bias.assign_add(LEARNING_RATE *
                                              tf.reduce_mean(input_placeholder - visible_reconstruction, 0))
hidden_bias_update = hidden_bias.assign_add(LEARNING_RATE *
                                            tf.reduce_mean(hidden_activation - final_hidden_activation, 0))

train_op = tf.group(weight_update, visible_bias_update, hidden_bias_update)

loss_op = tf.reduce_sum(tf.square(input_placeholder - visible_reconstruction))

session = tf.Session()

session.run(tf.global_variables_initializer())

current_epochs = 0

for i in range(10):
    total_loss = 0
    while mnist.train.epochs_completed == current_epochs:
        batch_inputs, batch_labels = mnist.train.next_batch(100)

        # print(type(batch_inputs),type(batch_labels), batch_inputs.shape, batch_labels.shape )
        # <class 'numpy.ndarray'> < class 'numpy.ndarray' > (100, 784) (100, )


        _, reconstruction_loss = session.run([train_op, loss_op], feed_dict={input_placeholder: batch_inputs })
        total_loss += reconstruction_loss

    print("epochs %s loss %s" % (current_epochs, reconstruction_loss))
    current_epochs = mnist.train.epochs_completed


#PP = np.zeros((1, 784))
PP = np.random.rand(1,784)

reconstruction = session.run(visible_reconstruction, feed_dict={input_placeholder: PP})
reconstruction_reshape = reconstruction.reshape(28, 28)

plt.imshow(reconstruction_reshape)
plt.show()



#
# for prob in [0.5, 0.6, 0.7, 0.8]:
#     for id in range(10):
#         Y = mnist.test.images[id].reshape(28, 28).copy()
#         label0 = mnist.test.labels[id]
#
#         plt.imshow(Y)
#         plt.savefig(Desktop + '234/' + str(label0) + '_' + str(prob) +'_a.png')
#
#
#         Z = figure_noise(Y, prob)
#         plt.imshow(Z)
#         plt.savefig(Desktop + '234/' + str(label0) + '_' + str(prob) + '_b.png')
#
#         Z = Z.reshape(1, 784)
#
#
#         reconstruction = session.run(visible_reconstruction, feed_dict={input_placeholder: Z})
#
#         reconstruction_reshape = reconstruction.reshape(28, 28)
#
#         plt.imshow(reconstruction_reshape)
#         plt.savefig(Desktop + '234/' + str(label0) + '_' + str(prob) + '_c.png')


