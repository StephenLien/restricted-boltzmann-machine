import time
import sys
import random
import numpy as np
import matplotlib.pyplot as plt
import pickle
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import preprocessing



pd.set_option('display.width', 1000)
pd.set_option('display.max_colwidth', 160)
np.set_printoptions(linewidth=10000)

########################################################################################################################
class Dataset_ftr:
    '''
    The first and second (3rd and 4th,...) mini-batch correspond to one whole epoch.
    This Dataset has no label.


    Example:

    Xtr, Ytr = np.arange(0, 10), np.arange(0, 100).reshape(10, 10)

    print(Xtr)
    print(Ytr)


    dataset1 = Dataset_ftr(Xtr)
    print("next batch of Xtr")
    for i in range(4):
        print(dataset1.next_batch(5))


    dataset2 = Dataset_ftr(Ytr)
    print("next batch of Ytr")
    for i in range(4):
        print(dataset2.next_batch(5))
    '''

    def __init__(self,data):
        self._index_in_epoch = 0
        self._epochs_completed = 0
        self._data = data
        self._num_examples = data.shape[0]
        pass


    @property
    def data(self):
        return self._data

    @property
    def epochs_completed(self):
        return self._epochs_completed

    def next_batch(self,batch_size,shuffle = True):
        start = self._index_in_epoch
        if start == 0 and self._epochs_completed == 0:
            idx = np.arange(0, self._num_examples)  # get all possible indexes
            np.random.shuffle(idx)  # shuffle indexe
            self._data = self.data[idx]  # get list of `num` random samples

        # go to the next batch
        if start + batch_size > self._num_examples:
            self._epochs_completed += 1
            rest_num_examples = self._num_examples - start
            data_rest_part = self.data[start:self._num_examples]
            idx0 = np.arange(0, self._num_examples)  # get all possible indexes
            np.random.shuffle(idx0)  # shuffle indexes
            self._data = self.data[idx0]  # get list of `num` random samples

            start = 0
            self._index_in_epoch = batch_size - rest_num_examples #avoid the case where the #sample != integar times of batch_size
            end =  self._index_in_epoch
            data_new_part =  self._data[start:end]
            return np.concatenate((data_rest_part, data_new_part), axis=0)
        else:
            self._index_in_epoch += batch_size
            end = self._index_in_epoch
            return self._data[start:end]

########################################################################################################################
